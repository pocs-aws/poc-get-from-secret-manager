const aws = require('aws-sdk');

const assumeRole = async () => {
    const STS = aws.STS;
    try {
        const sts = new STS();
        const role = process.env.ROLE_GET_SECRET;
        const data = await sts
            .assumeRole({
                RoleArn: role,
                RoleSessionName: 'sdk-aws',
            })
            .promise();
        if (!data.Credentials) {
            throw new Error('Invalid credentials');
        }
        const roleCredentials = {
            accessKeyId: data.Credentials.AccessKeyId,
            secretAccessKey: data.Credentials.SecretAccessKey,
            sessionToken: data.Credentials.SessionToken,
        };
        console.log('ASSUMED ROLE: TOKEN GENERATED:', role);
        return roleCredentials;
    } catch (error) {
        console.log(error.message);
        throw new Error(error);
    }
};

const getCredentialFromSecret = async () => {
    const SecretsManager = aws.SecretsManager;
    try {

        const roleCredentials = await assumeRole();
        const secretId = process.env.SECRET_ARN;
        const secretsManager = new SecretsManager({
            region: process.env.REGION,
            ...roleCredentials
        });

        console.log('secretId', secretId);
        const describeSecret = await secretsManager
            .describeSecret({
                SecretId: secretId
            })
            .promise();
        console.log('describeSecret', describeSecret);
        const secretValue = await secretsManager
            .getSecretValue({
                SecretId: secretId
            })
            .promise();
        console.log('secret value', secretValue);
        console.log('secret SecretString value', secretValue.SecretString);
        return secretValue;
    } catch (err) {
        console.error(err.message);
        throw err;
    }
};


module.exports.handler = async (event) => {
    console.log(event);
    const credentials = await getCredentialFromSecret();
    console.log('Credenciales obtenidas en runtine', credentials);
};
